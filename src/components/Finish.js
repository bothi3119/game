import Confetti from "react-confetti";

function Finish() {
  const size = {
    width: window.innerWidth || 300,
    height: window.innerHeight || 200,
  };
  return <Confetti width={size.width} height={size.height} />;
}

export default Finish;
