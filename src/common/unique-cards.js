const UNIQUE_CARDS = [
  {
    type: "CocaCola",
    image: require(`../images/CocaCola.png`),
  },
  {
    type: "Fanta",
    image: require(`../images/Fanta.png`),
  },
  {
    type: "Sprite",
    image: require(`../images/Sprite.png`),
  },
  {
    type: "Ciel",
    image: require(`../images/Ciel.png`),
  },
  {
    type: "SantaClara",
    image: require(`../images/SantaClara.png`),
  },
  {
    type: "DelValle",
    image: require(`../images/DelValle.png`),
  },
];

export default UNIQUE_CARDS;
