import { useEffect, useState, useRef } from "react";
import Button from "@mui/material/Button";
import RestartAltOutlinedIcon from "@mui/icons-material/RestartAltOutlined";
import Chip from "@mui/material/Chip";
import Stack from "@mui/material/Stack";
import Card from "./components/Card";
import Finish from "./components/Finish";
import "./App.scss";
import uniqueCardsArray from "./common/unique-cards";
import Badge from "@mui/material/Badge";
import { styled } from "@mui/material/styles";

function shuffleCards(array) {
  const length = array.length;
  for (let i = length; i > 0; i--) {
    const randomIndex = Math.floor(Math.random() * i);
    const currentIndex = i - 1;
    const temp = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temp;
  }
  return array;
}

export default function App() {
  const [cards, setCards] = useState(() =>
    shuffleCards(uniqueCardsArray.concat(uniqueCardsArray))
  );
  const [openCards, setOpenCards] = useState([]);
  const [clearedCards, setClearedCards] = useState({});
  const [shouldDisableAllCards, setShouldDisableAllCards] = useState(false);
  const [moves, setMoves] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const [bestScore, setBestScore] = useState(
    JSON.parse(localStorage.getItem("bestScore")) || 0
  );
  const timeout = useRef(null);

  const disable = () => {
    setShouldDisableAllCards(true);
  };
  const enable = () => {
    setShouldDisableAllCards(false);
  };

  const checkCompletion = () => {
    if (Object.keys(clearedCards).length === uniqueCardsArray.length) {
      setShowModal(true);
      const highScore = Math.min(moves, bestScore);
      setBestScore(highScore);
      localStorage.setItem("bestScore", highScore);
    }
  };

  const evaluate = () => {
    const [first, second] = openCards;
    enable();
    if (cards[first].type === cards[second].type) {
      setClearedCards((prev) => ({ ...prev, [cards[first].type]: true }));
      setOpenCards([]);
      return;
    }
    timeout.current = setTimeout(() => {
      setOpenCards([]);
    }, 500);
  };

  const handleCardClick = (index) => {
    console.log(showModal);
    if (openCards.length === 1) {
      setOpenCards((prev) => [...prev, index]);
      setMoves((moves) => moves + 1);
      disable();
    } else {
      clearTimeout(timeout.current);
      setOpenCards([index]);
    }
  };

  useEffect(() => {
    let timeout = null;
    if (openCards.length === 2) {
      timeout = setTimeout(evaluate, 300);
    }
    return () => {
      clearTimeout(timeout);
    };
  }, [openCards]);

  useEffect(() => {
    checkCompletion();
  }, [clearedCards]);
  const checkIsFlipped = (index) => {
    return openCards.includes(index);
  };

  const checkIsInactive = (card) => {
    return Boolean(clearedCards[card.type]);
  };

  const handleRestart = () => {
    setClearedCards({});
    setOpenCards([]);
    setMoves(0);
    setShowModal(false);
    setShouldDisableAllCards(false);
    setCards(shuffleCards(uniqueCardsArray.concat(uniqueCardsArray)));
  };

  const handleClick = () => {
    //@TODO
  };

  const handleDelete = () => {
    //@TODO
  };

  const StyledBadge = styled(Badge)(({ theme }) => ({
    "& .MuiBadge-badge": {
      right: -8,
      top: 0,
      border: `2px solid ${theme.palette.background.paper}`,
      padding: "0 4px",
    },
  }));

  return (
    <div className="App">
      <header>
        <div className="score">
          <Stack direction="row" spacing={2}>
            <Chip
              label="MOVIMIENTOS:"
              onClick={handleClick}
              onDelete={handleDelete}
              deleteIcon={
                <StyledBadge
                  badgeContent={moves ? moves : "0"}
                  color="secondary"
                ></StyledBadge>
              }
              variant="outlined"
            />
            <Chip
              label="MEJOR PUNTAJE:"
              onClick={handleClick}
              onDelete={handleDelete}
              deleteIcon={
                <StyledBadge
                  badgeContent={bestScore ? bestScore : "0"}
                  color="secondary"
                ></StyledBadge>
              }
              variant="outlined"
            />
          </Stack>
        </div>
      </header>
      <div className="container">
        {cards.map((card, index) => {
          return (
            <Card
              key={index}
              card={card}
              index={index}
              isDisabled={shouldDisableAllCards}
              isInactive={checkIsInactive(card)}
              isFlipped={checkIsFlipped(index)}
              onClick={handleCardClick}
            />
          );
        })}
      </div>
      <footer>
        <div className="restart">
          <Button
            onClick={handleRestart}
            variant="outlined"
            size="medium"
            color="secondary"
            startIcon={<RestartAltOutlinedIcon />}
          >
            Intentar de nuevo
          </Button>
        </div>
      </footer>
      {showModal ? <Finish /> : null}
    </div>
  );
}
